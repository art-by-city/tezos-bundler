# Art By City - Tezos Bundler

## Description
This project offers a REST API for accepting Tezos wallet keypair signed Arweve [ANS-104 Bundles](https://github.com/joshbenaron/arweave-standards/blob/ans104/ans/ANS-104.md).

## Requirements
- `node` `v16.13.2`

## Dev Requirements
- [ArLocal](https://github.com/textury/arlocal)
If developing locally, ensure the Arweave wallet passed to `TezosBundler` has an AR balance by calling `/mint/<address>/<amount>` on the arlocal API.

## Install
```bash
$ npm i --save @tezos-bundler
```

## Run Programmatically
```typescript
import Arweave from 'arweave'
import TezosBundler from 'tezos-bundler'

// Import your Arweave JWK keyfile
import arweaveJWK from './my-arweave-keyfile.json'

const arweave = new Arweave({ /* Arweave API Config */ })
const tezosBundler = new TezosBundler(arweaveJWK, arweave)
await tezosBundler.start()
```

Or with NuxtJS/NextJS/etc Server Middleware
```typescript
import Arweave from 'arweave'
import TezosBundler from 'tezos-bundler'

// Import your Arweave JWK keyfile
import arweaveJWK from './my-arweave-keyfile.json'

const arweave = new Arweave({ /* Arweave API Config */ })
const tezosBundler = new TezosBundler(testweaveJWK, arweave)
const callback = tezosBundler.app.callback()

export default callback
```

## Rest API
- `POST /bundle/xtz` Post an ANS-104 binary bundle to be posted to Arweave
  - Status `200`
  - Body
    ```
    {
      txid: 'arweave-tx-id',
      status: 200,
      statusText: 'OK'
    }
    ```
