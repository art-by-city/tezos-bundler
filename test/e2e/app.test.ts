import chai from 'chai'
import chaiHttp from 'chai-http'

import { tezosBundler } from './setup'

chai.use(chaiHttp)
const expect = chai.expect

describe('Tezos Bundler', () => {
  it('returns healthcheck', async () => {
    const route = '/healthcheck'

    try {
      const res = await chai.request(tezosBundler.server).get(route)

      expect(res).to.have.status(200)
      expect(res).to.be.json
    } catch (err) {
      expect.fail(err)
    }
  })
})
