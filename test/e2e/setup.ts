import ArLocal from 'arlocal'
import axios from 'axios'
import Arweave from 'arweave'

import testweaveJWK from './testweave-keyfile.json'
import TezosBundler from '../../src/app'

export const KEYS = {
  TEZOS: {
    alice: {
      address: 'tz1VSUr8wwNhLAzempoch5d6hLRiTh8Cjcjb',
      pk: 'edpkvGfYw3LyB1UcCahKQk4rF2tvbMUk8GFiTuMjL75uGXrpvKXhjn',
      sk: 'edsk3QoqBuvdamxouPhin7swCvkQNgq4jP5KZPbwWNnwdZpSpJiEbq'
    },
    bob: {
      address: 'tz1aSkwEot3L2kmUvcoxzjMomb9mvBNuzFK6',
      pk: 'edpkurPsQ8eUApnLUJ9ZPDvu98E8VNj4KtJa1aZr16Cr5ow5VHKnz4',
      sk: 'edsk3RFfvaFaxbHx8BMtEW1rKQcPtDML3LXjNqMNLCzC3wLC1bWbAt'
    }
  }
}

export const arlocal = new ArLocal(1984, false)
export const arweave = new Arweave({
  protocol: 'http',
  host: 'localhost',
  port: '1984'
})
export const tezosBundler = new TezosBundler(testweaveJWK, arweave)

export async function mine(blocks: number = 1) {
  try {
    await axios.get(`http://localhost:1984/mine/${blocks}`)
  } catch (err) {
    console.error('Error mining ArLocal in test setup', err)
  }
}

export async function mint(address: string, amount: string) {
  try {
    await axios.get(`http://localhost:1984/mint/${address}/${amount}`)
  } catch (err) {
    console.error('Error minting AR tokens in test setup', err)
  }
}

async function start() {
  await arlocal.start()
  await tezosBundler.start()
  const testweaveWallet = await arweave.wallets.jwkToAddress(testweaveJWK)

  // Mint tokens to Testweave Wallet
  await mint(testweaveWallet, '184717954005648')

  // Mine a block
  await mine()
}

async function stop() {
  await tezosBundler.stop()
  await arlocal.stop()
}

before(async () => {
  await start()
})

after(async () => {
  await stop()
})

process.on('SIGINT', () => { stop() })
process.on('SIGTERM', () => { stop() })
