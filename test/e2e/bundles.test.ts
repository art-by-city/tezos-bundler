import chai from 'chai'
import chaiHttp from 'chai-http'
import {
  Bundle,
  bundleAndSignData,
  createData,
  TezosSigner
} from 'tezos-arbundles'
import Arweave from 'arweave'

import { arweave, KEYS, tezosBundler } from './setup'

chai.use(chaiHttp)
const expect = chai.expect

async function createBundle(data: string | Uint8Array): Promise<Bundle> {
  const signer = new TezosSigner(KEYS.TEZOS.alice.sk)
  await signer.setPublicKey()
  const dataItem = createData(data, signer)
  await dataItem.sign(signer)
  const bundle = await bundleAndSignData([ dataItem ], signer)

  return bundle
}

describe('Bundles', () => {
  it('accepts ans-104 bundles', async () => {
    const route = '/bundle/xtz'
    const dataToPost = 'test string'

    try {
      const bundle = await createBundle(dataToPost)
      const res = await chai
        .request(tezosBundler.server)
        .post(route)
        .send(bundle.getRaw())
        .set('Content-Type', 'application/octet-stream')

      expect(res).to.have.status(200)
      expect(res).to.be.json
      expect(res.body).to.have.property('txid')
      expect(res.body).to.have.property('status')
      expect(res.body).to.have.property('statusText')

      const txid = res.body.txid
      expect(txid).to.have.length(43)

      const data = await arweave.transactions.getData(txid, { decode: true })
      const fetchedBundle = new Bundle(Buffer.from(data))
      expect(fetchedBundle.items).to.have.length(1)
      const dataFromBundle = Arweave.utils.b64UrlToString(
        fetchedBundle.items[0].data
      )
      expect(dataToPost).to.equal(dataFromBundle)
    } catch (err) {
      expect.fail(err)
    }
  })

  it('rejects payloads that are not valid ans-104 bundles', async () => {
    const route = '/bundle/xtz'

    try {
      const res = await chai
        .request(tezosBundler.server)
        .post(route)
        .send(Buffer.from('test'))
        .set('Content-Type', 'application/octet-stream')

      expect(res).to.have.status(400)
    } catch (err) {
      expect.fail(err)
    }
  })
})