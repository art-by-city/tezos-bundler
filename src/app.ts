import 'dotenv/config'
import Koa from 'koa'
import Router from '@koa/router'
import { Server } from 'http'
import { JWKInterface } from 'arweave/node/lib/wallet'
import Arweave from 'arweave'

import { BundlesRouter } from './router'

export default class TezosBundler {
  private port: number = 1985
  private arweaveKeyfile!: JWKInterface
  private arweave!: Arweave
  server!: Server
  app: Koa = new Koa()

  constructor(arweaveKeyfile: JWKInterface, arweave: Arweave) {
    this.arweave = arweave
    this.arweaveKeyfile = arweaveKeyfile
    this.build()
  }

  private build() {
    const router = new Router()

    router.get('/healthcheck', (ctx) => {
      ctx.body = { health: 'ok' }

      return
    })

    const bundlesRouter = new BundlesRouter(
      this.arweaveKeyfile,
      this.arweave
    )
    router.use(
      '/bundle',
      bundlesRouter.router.routes(),
      bundlesRouter.router.allowedMethods()
    )

    this.app
      .use(async (ctx, next) => {
        ctx.set('Access-Control-Allow-Origin', '*')

        await next()
      })
      .use(router.routes())
      .use(router.allowedMethods())
  }

  async start() {
    if (!this.server) {
      this.server = this.app.listen(this.port, () => {
        console.log(`Tezos Bundler listening on ${this.port}`)
      })
    }
  }

  async stop() {
    if (this.server) {
      this.server.close(() => console.log('Tezos Bundler stopped'))
    }
  }
}
