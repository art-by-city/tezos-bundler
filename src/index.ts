import Arweave from 'arweave'
import TezosBundler from './app'
import testweaveJWK from '../test/e2e/testweave-keyfile.json'

const ARWEAVE_PROTOCOL = process.env.ARWEAVE_PROTOCOL || 'http'
const ARWEAVE_HOST = process.env.ARWEAVE_HOST || 'localhost'
const ARWEAVE_PORT = process.env.ARWEAVE_PORT
  ? Number.parseInt(process.env.ARWEAVE_PORT)
  : 1984

;(async () => {
  const tezosBundler = new TezosBundler(
    testweaveJWK,
    new Arweave({
      protocol: ARWEAVE_PROTOCOL,
      host: ARWEAVE_HOST,
      port: ARWEAVE_PORT
    })
  )

  await tezosBundler.start()

  process.on('SIGINT', () => { tezosBundler.stop() })
  process.on('SIGTERM', () => { tezosBundler.stop() })
})()
