import Router from '@koa/router'
import { JWKInterface } from 'arweave/node/lib/wallet'
import getRawBody from 'raw-body'
import { Bundle } from 'tezos-arbundles'
import Arweave from 'arweave'

export default class BundlesRouter {
  router: Router = new Router()
  private arweaveKeyfile!: JWKInterface
  private arweave!: Arweave

  constructor(arweaveKeyfile: JWKInterface, arweave: Arweave) {
    this.arweave = arweave
    this.arweaveKeyfile = arweaveKeyfile
    this.build()
  }

  private build() {
    this.router.post('/xtz', async (ctx) => {
      const body = await getRawBody(ctx.req)
      console.log('[Tezos Bundler] got bundle body', body.length)
      const bundle = new Bundle(body)
      console.log('[Tezos Bundler] got bundle')
      const isValidBundle = (await bundle.verify()) && bundle.items.length > 0
      console.log('[Tezos Bundler] is bundle valid?', isValidBundle)
      if (!isValidBundle) {
        ctx.status = 400
        ctx.body = 'Not a valid ANS-104 Bundle'
        return
      }

      const tx = await this.arweave.createTransaction({ data: body })
      tx.addTag('App-Name', 'tezos-bundler')
      tx.addTag('App-Version', '1.0.0')
      tx.addTag('Bundle-Format', 'binary')
      tx.addTag('Bundle-Version', '2.0.0')

      await this.arweave.transactions.sign(tx, this.arweaveKeyfile)
      const { status, statusText } = await this.arweave.transactions.post(tx)

      ctx.body = { txid: tx.id, status, statusText }

      return
    })
  }
}
